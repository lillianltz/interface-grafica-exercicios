package projetojava;

import java.awt.BorderLayout;
import java.awt.Container;

import javax.swing.JButton;
import javax.swing.JFrame;

public class MyFrame extends JFrame {


	public MyFrame() {
			
		super("My Frame");//título do frame

		JButton button = new JButton("Clique aqui");
		
		Container x = getContentPane();
		x.add(button);
		x.add(BorderLayout.NORTH, new JButton("North"));
		x.add(BorderLayout.SOUTH, new JButton("South"));
		
		setSize(300, 300);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //permite  que o usuário feche o frame ao clicar no x.
		
	}
}
